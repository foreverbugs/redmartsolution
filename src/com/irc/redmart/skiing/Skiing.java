package com.irc.redmart.skiing;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Skiing Solution
 */
public class Skiing {

  private final int rows;
  private final int cols;
  private List<Vertex> vertices;


  public Skiing(List<Vertex> vertices, int rows, int cols) {
    this.vertices = vertices;
    this.rows = rows;
    this.cols = cols;
  }

  private Vertex getVertex(int row, int col) {
    if (row >= 0 && row < this.rows && col >= 0 && col < this.cols) {
      return this.vertices.get(row * this.cols + col);
    }
    return null;
  }

  /**
   * Returns neighbors of vertex a
   */
  private Vertex[] getNeighbors(Vertex v) {

    Vertex northNeighbor = getVertex(v.row - 1, v.column);
    Vertex eastNeighbor = getVertex(v.row, v.column + 1);
    Vertex southNeighbor = getVertex(v.row + 1, v.column);
    Vertex westNeighbor = getVertex(v.row, v.column - 1);

    return new Vertex[]{
        northNeighbor,
        eastNeighbor,
        southNeighbor,
        westNeighbor
    };
  }

  /**
   * Returns out-degree of vertex
   */
  private int calculateOutDegree(Vertex v, Vertex[] neighbors) {
    int outDegree = 0;
    for (Vertex neighbor : neighbors) {
      if (isDownhill(v, neighbor)) {
        outDegree++;
      }
    }
    return outDegree;
  }

  private boolean isDownhill(Vertex from, Vertex to) {
    if (from != null && to != null) {
      return from.elevation > to.elevation;
    }
    return false;
  }

  private boolean isViableParent(Vertex v, Vertex neighbor) {
    return neighbor != null && isDownhill(neighbor, v);
  }

  private Queue<Vertex> preProcess() {
    Queue<Vertex> zeroOutDegreeVertices = new LinkedList<>();

    for (Vertex vertex : this.vertices) {
      Vertex[] neighbors = getNeighbors(vertex);
      int outDegree = calculateOutDegree(vertex, neighbors);
      vertex.neighbors = neighbors;
      vertex.outDegree = outDegree;
      if (outDegree == 0) {
        zeroOutDegreeVertices.add(vertex);
      }
    }
    return zeroOutDegreeVertices;
  }

  /**
   * Find max path using Topo-Sort using Kahn's algo
   * (This works for very large number of vertices)
   */
  public void run() {

    Queue<Vertex> topoSortQueue = preProcess();
    int maxPathLength = 0;
    int maxElevationDelta = 0;

    while (!topoSortQueue.isEmpty()) {
      Vertex vertex = topoSortQueue.remove();
      if (vertex.lastSlopeVertex == null) {
        vertex.lastSlopeVertex = vertex;
      }

      int slopeLength = vertex.slopeLength + 1;
      int pathDelta = vertex.elevation - vertex.lastSlopeVertex.elevation;

      if (vertex.slopeLength > maxPathLength) {
        maxPathLength = vertex.slopeLength;
        maxElevationDelta = pathDelta;
      } else if (vertex.slopeLength == maxPathLength && maxElevationDelta < pathDelta) {
        maxElevationDelta = pathDelta;
      }

      for (Vertex neighbor : vertex.neighbors) {
        if (isViableParent(vertex, neighbor)) {
          neighbor.outDegree--;
          if (neighbor.outDegree == 0) {
            topoSortQueue.add(neighbor);
          }

          if (neighbor.slopeLength < slopeLength ||
              (neighbor.slopeLength == slopeLength &&
                  neighbor.lastSlopeVertex.elevation > vertex.lastSlopeVertex.elevation)) {
            neighbor.slopeLength = slopeLength;
            neighbor.lastSlopeVertex = vertex.lastSlopeVertex;
          }
        }
      }
    }

    System.out.println(String.format("path:%d elevd:%d", maxPathLength + 1, maxElevationDelta));
  }

  @Data
  @RequiredArgsConstructor
  @ToString
  public static class Vertex {

    private final int row;
    private final int column;
    private final int elevation;

    private int outDegree;
    private Vertex[] neighbors;
    private int slopeLength = 0;
    private Vertex lastSlopeVertex = null;
  }
}

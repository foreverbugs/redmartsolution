package com.irc.redmart.skiing;

import com.irc.redmart.skiing.Skiing.Vertex;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SkiingMain {

  public static void main(String[] args) throws IOException {

    long startTime = System.currentTimeMillis();

    try (BufferedReader br = new BufferedReader(new FileReader("/home/epoch/Desktop/map_.txt"))) {
      StringTokenizer st = new StringTokenizer(br.readLine());
      int cols = Integer.parseInt(st.nextToken());
      int rows = Integer.parseInt(st.nextToken());

      List<Vertex> vertices = new ArrayList<>(rows * cols);

      for (int i = 0; i < rows; i++) {
        int j = 0;
        st = new StringTokenizer(br.readLine());
        while (st.hasMoreTokens()) {
          vertices.add(new Vertex(i, j++, Integer.parseInt(st.nextToken())));
        }
      }

      Skiing skiing = new Skiing(vertices, rows, cols);
      skiing.run();
      long endTime = System.currentTimeMillis();
      System.out.println("Runtime:" + (endTime - startTime) + " ms");
    }
  }
}
